package de.jreality.plugin.basic;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

import de.jreality.jogl.InstrumentedViewer;
import de.jreality.jogl3.JOGL3Viewer;
import de.jreality.jogl3.helper.PostProcessingHelper;
import de.jreality.plugin.icon.ImageHook;
import de.jreality.ui.viewerapp.ViewerSwitch;
import de.jreality.ui.viewerapp.actions.file.Quit;
import de.jtem.jrworkspace.plugin.Controller;
import de.jtem.jrworkspace.plugin.PluginInfo;
import de.jtem.jrworkspace.plugin.aggregators.MenuAggregator;
import de.jtem.jrworkspace.plugin.flavor.PerspectiveFlavor;
import de.jtem.jrworkspace.plugin.flavor.ShutdownFlavor;

public class ViewMenuBar extends MenuAggregator implements ShutdownFlavor {

	/**
	 * the main menu bar of the jreality window (file, viewer, window etc..)
	 */
	private View viewerPlugin = null;
	private ShutdownListener shutdownListener;

	@Override
	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Viewer Menu";
		info.vendorName = "jReality Group";
		info.icon = ImageHook.getIcon("menu.png");
		return info;
	}

	@Override
	public void install(Controller c) throws Exception {
		viewerPlugin = c.getPlugin(View.class);
		removeAll(getClass());

		// File menu
		JMenu fileMenu =  new JMenu("File");
		fileMenu.setMnemonic('f');
		addMenu(getClass(), 0.0, fileMenu);

		addMenuSeparator(getClass(), 99, "File");
		Quit quitMenuAction = new Quit("Quit");
		quitMenuAction.setActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				shutdownListener.shutdown();
			}
		});
		addMenuItem(getClass(), 100, quitMenuAction, "File");

		// Viewer menu
		JMenu viewerMenu = viewerPlugin.createViewerMenu();
		viewerMenu.setMnemonic('v');
		addMenu(getClass(), 1.0, viewerMenu);
		addMenuSeparator(getClass(), 0.0, "Viewer");

		// window menu
		JMenu windowMenu = new JMenu("Window");
		windowMenu.setMnemonic('w');
		addMenu(getClass(), 100.0, windowMenu);
		addMenuItem(getClass(), 1.0, viewerPlugin.getPanelsMenu(), "Window");
		addMenuSeparator(getClass(), 1.5, "Window");

		// here a few extra menues defined in the View plugin
		JMenu slotsMenu = viewerPlugin.getContaintersMenu();
		double priority = 20.0;
		addMenuSeparator(getClass(), 19.5, "Window");
		for (Component item : slotsMenu.getMenuComponents()) {
			addMenuItem(getClass(), priority++, (JMenuItem)item, "Window");
		}

		// post processing added here for jogl3
		//check if jogl3 is present
		for(int i = 0 ; i < viewerPlugin.viewerSwitch.getViewers().length ; i++){
			if(viewerPlugin.viewerSwitch.getViewers()[i] instanceof JOGL3Viewer){
				// add the menue
				double priorityEf = 10000.0;
				String MenuName = "Effects (JOGL3)";
				JMenu postProcessingMenu = PostProcessingHelper.makeMenu(viewerPlugin, (JOGL3Viewer) viewerPlugin.viewerSwitch.getViewers()[i], MenuName);			
				postProcessingMenu.setMnemonic('e');
				addMenu(getClass(), priorityEf, postProcessingMenu);
				addMenuSeparator(getClass(), 0.0, MenuName);
			}
		}
		
}


		@Override
		public void uninstall(Controller c) throws Exception {

		}

		@Override
		public Class<? extends PerspectiveFlavor> getPerspective() {
			return View.class;
		}

		@Override
		public void setShutdownListener(ShutdownListener shutdownListener) {
			this.shutdownListener = shutdownListener;
		}



	}
